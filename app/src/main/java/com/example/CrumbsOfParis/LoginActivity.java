package com.example.CrumbsOfParis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.*; // uses Context, Intent, SharedPreferences
import android.os.Bundle;
import android.view.View;
import android.widget.*; // uses Button, EditText, TextView, Toast

public class LoginActivity extends AppCompatActivity {

    private EditText edEmail;
    private EditText edPassword;
    private Button btnLogin;
    private TextView btnSignUp;

    private final String CREDENTIAL_SHARED_PREF = "our_shared_pref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edEmail = findViewById(R.id.login_email);
        edPassword = findViewById(R.id.login_password);
        btnLogin = findViewById(R.id.login_button);
        btnSignUp = findViewById(R.id.login_signup);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences credentials = getSharedPreferences(CREDENTIAL_SHARED_PREF, Context.MODE_PRIVATE);
                String email = credentials.getString("Email", null);
                String password = credentials.getString("Password", null);

                String email_from_ed = edEmail.getText().toString();
                String password_from_ed = edPassword.getText().toString();

                if (email != null && email_from_ed != null && email.equalsIgnoreCase(email_from_ed)) {
                    if (password != null && password_from_ed != null && password.equalsIgnoreCase(password_from_ed)) {
                        Toast.makeText(LoginActivity.this, "Login Success!", Toast.LENGTH_SHORT).show();

                        /** Intent intent = new Intent(LoginActivity.this, HomepageActivity.class);
                         intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                         startActivity(intent);

                         finish(); **/
                    } else {
                        Toast.makeText(LoginActivity.this, "Login Failed!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Login Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}