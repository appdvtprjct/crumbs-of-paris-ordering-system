package com.example.CrumbsOfParis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SignUpActivity extends AppCompatActivity {

    private EditText suUsername;
    private EditText suEmail;
    private EditText suMoNum;
    private EditText suPassword;
    private EditText suCPassword;
    private Button btnRegister;
    private TextView btnSignIn;

    private final String CREDENTIAL_SHARED_PREF = "our_shared_pref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        suUsername = findViewById(R.id.reg_username);
        suEmail = findViewById(R.id.reg_email);
        suMoNum = findViewById(R.id.reg_mobileNum);
        suPassword = findViewById(R.id.reg_password);
        suCPassword = findViewById(R.id.confirmPassword);
        btnRegister = findViewById(R.id.reg_button);
        btnSignIn = findViewById(R.id.reg_signIn);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newUser = suUsername.getText().toString();
                String newEmail = suEmail.getText().toString();
                String newNum = suMoNum.getText().toString();
                String newPass = suPassword.getText().toString();
                String newCPass = suCPassword.getText().toString();

                if (newPass != null && newCPass != null && newPass.equalsIgnoreCase(newCPass)){
                    SharedPreferences credentials = getSharedPreferences(CREDENTIAL_SHARED_PREF, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = credentials.edit();
                    editor.putString("Password", newPass);
                    editor.putString("Email", newEmail);
                    editor.commit();

                    SignUpActivity.this.finish();
                }
            }
        });
    }
}